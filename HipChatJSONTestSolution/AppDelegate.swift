//
//  AppDelegate.swift
//  HipChatJSONTestSolution
//
//  Created by Mikhail Baynov on 02/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

let model = Model()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

   var window: UIWindow?


   func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

      window = UIWindow(frame: UIScreen.mainScreen().bounds)
      if let window = window {
         window.backgroundColor = UIColor.whiteColor()
         window.rootViewController = ViewController()
         window.makeKeyAndVisible()
      }
      return true
   }


}

