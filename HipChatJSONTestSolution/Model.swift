//
//  Model.swift
//  HipChatJSONTestSolution
//
//  Created by Mikhail Baynov on 02/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//


import Foundation

public class Model {
   
   public func processChatInput(input : String) -> String {
      var out = ""
      var matches = [String]()

      
      //mentions
      matches = applyRegex("(?<=\\s|^)@[A-Za-z0-9_]+", input: input)
      if matches.count > 0 {
         out += "{\"mentions\": ["
         for i in 0..<matches.count {
            if i > 0 {
               out += ", "
            }
            var a = matches[i]
            a = a.stringByReplacingOccurrencesOfString("@", withString:"\"")
            out += a
            out += "\""
         }
         out += "]}"
      }

      
      //emoticons
      matches = applyRegex("\\(\\w{1,15}\\)", input: input)
      if matches.count > 0 {
         if out != "" {
            out += ", "
         }
         out += "{\"emoticons\": ["
         for i in 0..<matches.count {
            if i > 0 {
               out += ", "
            }
            var a = matches[i]
            a = a.stringByReplacingOccurrencesOfString("(", withString:"\"")
            a = a.stringByReplacingOccurrencesOfString(")", withString:"\"")
            out += a
         }
         out += "]}"
      }

      
      //links
      matches = applyRegex("https?://(www.)?[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)", input: input)
      if matches.count > 0 {
         if out != "" {
            out += ", "
         }
         out += "{\"links\": ["
         for i in 0..<matches.count {
            if i > 0 {
               out += ", "
            }
            let url = matches[i]
            let title = getWebpageTitle(url)
            
            
            out += "{ \"url\": \"\(url)\", \"title\": \"\(title)\" }"
         }
         out += "]}"
      }

      
      return out
   }
   
   
   func applyRegex(regex:String, input:String) -> [String] {
      var out = [String]()
      do {
         let nsregex = try NSRegularExpression(pattern: regex, options: NSRegularExpressionOptions.CaseInsensitive)
         var matches = nsregex.matchesInString(input, options: [], range: NSMakeRange(0, input.characters.count)) as Array<NSTextCheckingResult>
         if matches.count > 0 {
            for i in 0..<matches.count {
               let range = matches[i].rangeAtIndex(0)
               let nsinput = input as NSString
               out.append(nsinput.substringWithRange(range))
            }
         }
      } catch {
         print("bad regex")
      }
      return out
   }
   
   
   
   func getWebpageTitle(url : String) -> String {
      var input = ""
      var out = ""
      let url = NSURL(string: url)
      let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
         if data != nil {
            input = NSString(data: data!, encoding: NSUTF8StringEncoding) as! String
            do {
               let regex = try NSRegularExpression(pattern: "<title>(.*)</title>", options: NSRegularExpressionOptions.CaseInsensitive)  //"<title>(.*)</title>"
               if let match = regex.firstMatchInString(input, options: [], range: NSMakeRange(0, input.characters.count)) {
                  let range = match.rangeAtIndex(0)
                  let nsinput = input as NSString
                  out = nsinput.substringWithRange(NSRange(location: range.location+7, length: range.length-15))
                  print(out)
               }
            } catch {
               // bad regex
            }
         }
      }
      task.resume()
      while(out == "") {
      }
      return out
   }



}



