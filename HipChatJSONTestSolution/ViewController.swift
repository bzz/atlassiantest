//
//  ViewController.swift
//  HipChatJSONTestSolution
//
//  Created by Mikhail Baynov on 02/05/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
   let bedView = UIView()
   let textField = UITextField()

   
   override func viewDidLoad() {
      super.viewDidLoad()
      view.backgroundColor = UIColor.lightGrayColor()
      
      
      textField.translatesAutoresizingMaskIntoConstraints = false

      
      bedView.translatesAutoresizingMaskIntoConstraints = false
      
      textField.translatesAutoresizingMaskIntoConstraints = false
      textField.backgroundColor = UIColor.whiteColor()
      textField.delegate = self
      textField.tag = 1
      textField.textAlignment = NSTextAlignment.Center
      textField.font = UIFont(name: "HelveticaNeue-Light", size: 14)
      textField.layer.cornerRadius = 5
      textField.placeholder = "Input chat string"
      textField.keyboardType = .URL
      textField.autocorrectionType = .No
      textField.autocapitalizationType = .None
      textField.becomeFirstResponder()
      
      view.addSubview(bedView)
      bedView.addSubview(textField)
      
      
      bedView.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor).active = true
      bedView.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor, constant: 10).active = true
      bedView.widthAnchor.constraintEqualToAnchor(nil, constant: 240).active = true
      bedView.heightAnchor.constraintEqualToAnchor(nil, constant: 100).active = true
      
      
      textField.centerXAnchor.constraintEqualToAnchor(bedView.centerXAnchor).active = true
      textField.centerYAnchor.constraintEqualToAnchor(bedView.centerYAnchor, constant: -48).active = true
      textField.widthAnchor.constraintEqualToAnchor(nil, constant: 240).active = true
      textField.heightAnchor.constraintEqualToAnchor(nil, constant: 32).active = true
   
      
      textField.text = "@bob  Rambler is silly (stupid)  @yandex ov@gmail.com Yandex http://www.ya.ru   Rambler http://www.rambler.ru"
   }
}

extension ViewController: UITextFieldDelegate {
   
   
   func textFieldShouldReturn(textField: UITextField) -> Bool {
      
      if (textField.text == "") {
         return false
      }
      
      
      textField.resignFirstResponder()
      
      print("Input: " + textField.text!)
      
      let output = model.processChatInput(textField.text!)
      print("Return: " + output)
      alert(output)
      
      textField.text = ""
      return true
   }
   
   
   override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
      textField.resignFirstResponder()
   }
   
   func alert(message: String) {
      let alert = UIAlertController(title: "Return from input", message:message, preferredStyle: .Alert)
      alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
      self.presentViewController(alert, animated: true){}
   }

   
}



